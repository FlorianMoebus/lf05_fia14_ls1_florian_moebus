import java.util.Scanner; // Import der Klasse Scanner 
 
public class Hello  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Hallo! Wie hei�en Sie: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    String zahl1 = myScanner.next();  
     
    System.out.print("Sch�ner Name, wie alt bist du denn?: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    String zahl2 = myScanner.next();  
     
    
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    String ergebnis = zahl1 + zahl2;  
     
    System.out.print("\n\n\nDeine Antworten sind: "); 
    System.out.print(" Name: " + zahl1 + "   " + "Alter: " +              zahl2);   
 
    myScanner.close(); 
     
  }    
} 